package com.example.demo;

import com.example.demo.services.Data;
import com.example.demo.services.DataServiceGrpc;
import io.grpc.stub.StreamObserver;
import org.lognet.springboot.grpc.GRpcService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Random;

@SpringBootApplication
public class DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @Bean
    public Data getData(Random random) throws IOException {
        String data = new String(Files.readAllBytes(Paths.get("src/main/resources/NBATVBlackout.xml")));
        StringBuffer buffer = new StringBuffer();
        for (int i = 1; i <= 10; i++) {
            buffer.append(random.nextInt() + ":" + data + "\n");
        }
        return Data.newBuilder().setText(buffer.toString()).build();
    }

    @Bean
    Random random() {
        return new Random();
    }
}

class SimpleData {
    String data;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}

@RestController
@RequestMapping("/")
class DataController {
    @Autowired
    Data data;
    @Autowired
    Random random;

    @RequestMapping(method = RequestMethod.GET)
    public SimpleData getData() {
        SimpleData simpleData = new SimpleData();
        simpleData.setData(random.nextInt() + ":" + data.getText());
        return simpleData;
    }
}

@GRpcService
class DataService extends DataServiceGrpc.DataServiceImplBase {
    @Autowired
    Data data;
    @Autowired
    Random random;

    @Override
    public void getData(Data request, StreamObserver<Data> responseObserver) {
        responseObserver.onNext(Data.newBuilder().setText(random.nextInt() + ":" + data.getText()).build());
        responseObserver.onCompleted();
    }
}
