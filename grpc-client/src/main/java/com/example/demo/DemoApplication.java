package com.example.demo;

import ch.qos.logback.core.util.FileUtil;
import com.example.demo.services.Data;
import com.example.demo.services.DataServiceGrpc.DataServiceBlockingStub;
import io.grpc.ManagedChannel;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import java.text.DecimalFormat;

import static com.example.demo.services.DataServiceGrpc.newBlockingStub;
import static io.grpc.ManagedChannelBuilder.forAddress;

@SpringBootApplication
public class DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }


    interface ServiceCall {
        long call();
    }

    @Bean
    CommandLineRunner clr() {
        return args -> {

//            RestTemplate template = new RestTemplate();
//            final String s = template.getForObject("http://localhost:8080", String.class);
//            System.out.println(s.getBytes().length);
//
            ManagedChannel channel = forAddress("localhost", 6565).usePlaintext().build();
            DataServiceBlockingStub stub = newBlockingStub(channel);

            System.out.println("\n>> Making GRPC calls:");
            throughput(() -> {
                final Data data = stub.getData(null);
                return data.getText().getBytes().length;
            }, 1, 10);

            RestTemplate template = new RestTemplate();

            System.out.println("\n>> Making REST calls:");
            throughput(() -> {
                final String s = template.getForObject("http://localhost:8080", String.class);
                return s.getBytes().length;
            }, 1, 10);

            System.out.println();
        };
    }


    void throughput(ServiceCall action, int warmup, int runTime) throws Exception {
        // warmup
        long stopTime = System.currentTimeMillis() + (warmup * 1000);
        while (System.currentTimeMillis() < stopTime) {
            action.call();
        }

        // real test
        long cumulativeProcessingTime = 0;
        long numCalls = 0;
        long startTime = System.nanoTime();
        long totalBytes = 0;
        stopTime = System.currentTimeMillis() + (runTime * 1000);

        while (System.currentTimeMillis() < stopTime) {
            long _s = System.nanoTime();
            totalBytes += action.call();
            long _t = System.nanoTime() - _s;
            cumulativeProcessingTime += _t;
            numCalls++;
        }

        // show results
        DecimalFormat df = new DecimalFormat("#0.00");
        System.out.println("Throughput (calls/sec): "
                + df.format(numCalls / ((System.nanoTime() - startTime) / (double) 1000000000)));
        System.out.println("Mean Latency (ms): "
                + df.format((cumulativeProcessingTime / 1000000) / (double) numCalls));
        System.out.println("Total bytes read: " + totalBytes + " = " + humanReadableByteCount(totalBytes, true));
    }

    static String humanReadableByteCount(long bytes, boolean si) {
        int unit = si ? 1000 : 1024;
        if (bytes < unit) return bytes + " B";
        int exp = (int) (Math.log(bytes) / Math.log(unit));
        String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp-1) + (si ? "" : "i");
        return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
    }
}
