// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: DataService.proto

package com.example.demo.services;

public interface DataOrBuilder extends
    // @@protoc_insertion_point(interface_extends:com.example.demo.services.Data)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>string text = 1;</code>
   */
  java.lang.String getText();
  /**
   * <code>string text = 1;</code>
   */
  com.google.protobuf.ByteString
      getTextBytes();
}
